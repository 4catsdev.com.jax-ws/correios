# correios
Um exemplo de consulta de endereço de uma localidade por meio do CEP. A aplicação utiliza o serviço SOAP fornecido pelos Correios. A aplicação foi desenvolvida usando java desktop e possui uma classe main para fazer a consulta do endereço.

Vídeo no youtube: https://youtu.be/gPKKKw8uGu0

#### Características:
- A requisição entre a aplicação e o serviço do correio é feita por meio do método main;
- É usado o jaxws-maven-plugin para gerar as classes dos correios no cliente;
- É usado a versão 3.0.0 do jax-ws
